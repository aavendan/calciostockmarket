package info.bliki.api;


import info.bliki.api.Connector;
import info.bliki.api.Page;
import info.bliki.api.SearchResult;
import info.bliki.api.User;
import info.bliki.api.XMLSearchParser;
import info.bliki.api.creator.DocumentCreator;
import info.bliki.api.creator.HTMLConstants;
import info.bliki.api.creator.ImageData;
import info.bliki.api.creator.TopicData;
import info.bliki.api.creator.WikiDB;
import info.bliki.api.query.OpenSearch;
import info.bliki.api.query.RequestBuilder;
import info.bliki.wiki.filter.Encoder;
import info.bliki.wiki.filter.PlainTextConverter;
import info.bliki.wiki.filter.TemplateParser;
import info.bliki.wiki.filter.WikipediaParser;
import info.bliki.wiki.impl.APIWikiModel;
import info.bliki.wiki.model.SoccerTeamWikiModel;
import info.bliki.wiki.model.WikiModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.xml.sax.SAXException;

public class Timeline {

    public static String[] DISAMBIGUATORS = {"football","soccer"};
    public static final String TEST = "This is a [[Hello World]] '''example'''";

    /**
     * @param args
     */
    public static void main(String[] args) {
        User user = new User("", "", "http://it.wikipedia.org/w/api.php");
        Connector connector = new Connector();
        user = connector.login(user);

        RequestBuilder request = OpenSearch.create().search("sock").format("xml").maxlag(10);
        String jsonResponse = connector.sendXML(user, request);
        System.out.println(jsonResponse);
    }
    
    public static void main2(String[] args) {
        String name = "BENTIVOGLIO Simone";
        try {
            searchPlayerWikipedia(formatName(name));
//            searchDb();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public static void searchPlayerWikipedia(String name) throws JDOMException {
        User user = new User("", "", "http://en.wikipedia.org/w/api.php");
        user.login();
        ArrayList<String> strList;

        String[] valuePairs = { "list", "search", "srsearch", name };
        String[] valuePairsContinue = new String[6];
        String srOffset = "0";
        for (int i = 0; i < valuePairs.length; i++) {
            valuePairsContinue[i] = valuePairs[i];
        }
        valuePairsContinue[4] = "sroffset";
        valuePairsContinue[5] = "";
        Connector connector = new Connector();
        List<SearchResult> resultSearchResults = new ArrayList<SearchResult>(1024);
        XMLSearchParser parser;

        try {

            // get all search results
            String responseBody = connector.queryXML(user, valuePairs);
            while (responseBody != null) {
                parser = new XMLSearchParser(responseBody);
                parser.parse();
                srOffset = parser.getSrOffset();

                List<SearchResult> listOfSearchResults = parser.getSearchResultList();
                resultSearchResults.addAll(listOfSearchResults);

                for (SearchResult searchResult : listOfSearchResults) {
                    // print search result information
                    if(contains(searchResult.getTitle(), name) && disambiguation(searchResult.getSnippet())) {
                        //                        System.out.println(searchResult.toString());
                        

                        strList = new ArrayList<String>();
                        strList.add(searchResult.getTitle());
                        List<Page> listOfPages = user.queryContent(strList);

                        for (Page page : listOfPages) {
                            testWikipediaENAPI(searchResult.getTitle());
                        }

                    }
                }
                if (srOffset.length() > 0) {
                    // use the sroffset from the last query to get the next block of
                    // search results
                    valuePairsContinue[5] = srOffset;
                    responseBody = connector.queryXML(user, valuePairsContinue);
                } else {
                    break;
                }
            }

            

            

        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void searchDb() {
        WikiDB db = null;

        String mainDirectory = "/usr/local/wikipedia/";
        String databaseSubdirectory = "WikiDB";
        try {
            db = new WikiDB(mainDirectory, databaseSubdirectory);
//            System.out.println(db.selectTopic("Template:Infobox football biography").getContent());
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (db != null) {
                try {
                    db.tearDown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void testWikipediaENAPI(String title) {
        String[] listOfTitleStrings = {
                title
        };
        String titleURL = Encoder.encodeTitleLocalUrl(title);
        User user = new User("", "", "http://en.wikipedia.org/w/api.php");
        user.login();
        String mainDirectory = "/usr/local/wikipedia/";
        // the following subdirectory should not exist if you would like to create a
        // new database
        String databaseSubdirectory = "WikiDB";
        // the following directory must exist for image downloads
        String imageDirectory = "/usr/local/wikipedia/Images";
        // the generated HTML will be stored in this file name:
        String generatedHTMLFilename = mainDirectory + titleURL + ".html";

        WikiDB db = null;

        try {
            db = new WikiDB(mainDirectory, databaseSubdirectory);
            APIWikiModel wikiModel = new APIWikiModel(user, db, "${image}", "${title}", imageDirectory);
            DocumentCreator creator = new DocumentCreator(wikiModel, user, listOfTitleStrings);
            creator.setHeader(HTMLConstants.HTML_HEADER1 + HTMLConstants.CSS_SCREEN_STYLE + HTMLConstants.HTML_HEADER2);
            creator.setFooter(HTMLConstants.HTML_FOOTER);
            wikiModel.setUp();
            creator.renderToFile(generatedHTMLFilename);
//            db.getAllTopic();
            
//            System.out.println(db.selectTopic("Template:Infobox football biography").getContent());
            
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (db != null) {
                try {
                    db.tearDown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static boolean contains(String title, String name) {
        String[] names = name.split(" ");
        return title.contains(names[0]) || title.contains(names[1]);
    }

    public static boolean disambiguation(String snippet) {
        return snippet.contains(DISAMBIGUATORS[0]) || snippet.contains(DISAMBIGUATORS[1]);
    }

    public static String formatName(String name) {
        int i = 0;
        String nName = new String();

        for (char c : name.toCharArray()) {
            if (Character.isUpperCase(c) || Character.isSpaceChar(c)) 
                i++;
            else break;
        }

        nName = nName.concat(name.substring(i-1, name.length()));
        nName = nName.concat(" ");
        nName = nName.concat(titleCase(name.substring(0, i-1)));

        return nName;
    }

    public static String titleCase(String realName) {
        String space = " ";
        String[] names = realName.split(space);
        StringBuilder b = new StringBuilder();
        for (String name : names) {
            if (name == null || name.isEmpty()) {
                b.append(space);
                continue;
            }
            b.append(name.substring(0, 1).toUpperCase())
            .append(name.substring(1).toLowerCase())
            .append(space);
        }
        return b.toString();
    }

}
