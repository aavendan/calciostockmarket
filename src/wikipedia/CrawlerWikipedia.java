package wikipedia;


import info.bliki.api.Connector;
import info.bliki.api.Page;
import info.bliki.api.SearchResult;
import info.bliki.api.User;
import info.bliki.api.XMLSearchParser;
import info.bliki.api.creator.DocumentCreator;
import info.bliki.api.creator.HTMLConstants;
import info.bliki.api.creator.WikiDB;
import info.bliki.wiki.filter.Encoder;
import info.bliki.wiki.impl.APIWikiModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jfree.util.Log;
import org.xml.sax.SAXException;

import db.Player;
import db.PlayerDB;

public class CrawlerWikipedia {

    public static String WIKIPEDIA_URL_en = "http://en.wikipedia.org/w/api.php";
    public static String[] DISAMBIGUATORS_en = {"biography"};
    
    public static String WIKIPEDIA_URL_it = "http://it.wikipedia.org/w/api.php";
    public static String[] DISAMBIGUATORS_it = {"Carriera sportivo"};

    static String mainDirectory = "/usr/local/calcio/";
    static String databaseSubdirectory = "WikiDB";
    static String htmlDirectory = "/usr/local/calcio/html/";
    static String imageDirectory = htmlDirectory + "Images";

    public static WikiDB db = null;
    
    public static void CrawlWikipediaShorten(Player p) {
        try {
            db = new WikiDB(mainDirectory, databaseSubdirectory);
            searchPlayerWikipedia(p.getName()+" "+p.getSurname(),WIKIPEDIA_URL_en,DISAMBIGUATORS_en);
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (db != null) {
                try {
                    db.tearDown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static void CrawlWikipedia(PlayerDB playerDB, ArrayList<Player> players) throws Exception{

        db = new WikiDB(mainDirectory, databaseSubdirectory);

        try {
            for(Player p : players) {
                try {
                    if(searchPlayerWikipedia(p.getName()+" "+p.getSurname(),WIKIPEDIA_URL_it,DISAMBIGUATORS_it))
                        playerDB.updatePlayer(p);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } 

        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (db != null) {
                try {
                    db.tearDown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static boolean searchPlayerWikipedia(String name,String url, String[] desambiguators) throws Exception {
        User user = new User("", "", url);
        user.login();
        ArrayList<String> strList;

        String[] valuePairs = { "list", "search", "srsearch", name };
        String[] valuePairsContinue = new String[6];
        String srOffset = "0";
        for (int i = 0; i < valuePairs.length; i++) {
            valuePairsContinue[i] = valuePairs[i];
        }
        valuePairsContinue[4] = "sroffset";
        valuePairsContinue[5] = "";
        Connector connector = new Connector();
        List<SearchResult> resultSearchResults = new ArrayList<SearchResult>(1024);
        XMLSearchParser parser;
        
        boolean foundSF = false;

        try {

            // get all search results
            String responseBody = connector.queryXML(user, valuePairs);
            
            //If I get a response, I parse it
            while (responseBody != null) {
                parser = new XMLSearchParser(responseBody);
                parser.parse();
                srOffset = parser.getSrOffset();
                
                List<SearchResult> listOfSearchResults = parser.getSearchResultList();
                
                resultSearchResults.addAll(listOfSearchResults);

                for (SearchResult searchResult : listOfSearchResults) {
                    // print search result information
                    if(contains(searchResult.getTitle(), name)) {


                        strList = new ArrayList<String>();
                        strList.add(searchResult.getTitle());
                        List<Page> listOfPages = user.queryContent(strList);

                        for (Page page : listOfPages) {
                            if(disambiguation(page.getCurrentContent(),desambiguators)) {
                                testWikipediaENAPI(searchResult.getTitle(),url);
                                foundSF = true;
                            }
                        }
                        

                    }
                }
                
                //If for some reason none of the results is the soccer player
                //due to a mistake on the name or something else
                if(!foundSF) {
                    
                }
                
                if (srOffset.length() > 0) {
                    
                    
                    // use the sroffset from the last query to get the next block of
                    // search results
                    valuePairsContinue[5] = srOffset;
                    responseBody = connector.queryXML(user, valuePairsContinue);
                    
                    // Don't crawl long list of results
                    if(Integer.valueOf(srOffset) > 10) break;
                } else {
                    break;
                }
            }


            if(!foundSF) 
                System.out.println("NOT FOUND: "+name);
            else System.out.println("CRAWLED: "+name);



        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return foundSF;
    }

    public static void testWikipediaENAPI(String title,String url) throws Exception {
        String[] listOfTitleStrings = {
                title
        };
        String titleURL = Encoder.encodeTitleLocalUrl(title);
        User user = new User("", "", url);
        user.login();
        // the generated HTML will be stored in this file name:
        String generatedHTMLFilename = htmlDirectory + titleURL + ".html";

        try {

            APIWikiModel wikiModel = new APIWikiModel(user, db, "${image}", "${title}", null);
            DocumentCreator creator = new DocumentCreator(wikiModel, user, listOfTitleStrings);
            creator.setHeader(HTMLConstants.HTML_HEADER1 + HTMLConstants.CSS_SCREEN_STYLE + HTMLConstants.HTML_HEADER2);
            creator.setFooter(HTMLConstants.HTML_FOOTER);
            wikiModel.setUp();
            creator.renderToFile(generatedHTMLFilename);
        } catch (Exception ex) {
            Log.info(ex.getMessage());
        }

    }

    public static boolean contains(String title, String name) {
        String[] names = name.split(" ");
        return title.contains(names[0]) || title.contains(names[1]);
    }

    public static boolean disambiguation(String content,String[] disambiguators) {
        return content.contains(disambiguators[0]);
    }

    public static String titleCase(String realName) {
        String space = " ";
        String[] names = realName.split(space);
        StringBuilder b = new StringBuilder();
        for (String name : names) {
            if (name == null || name.isEmpty()) {
                b.append(space);
                continue;
            }
            b.append(name.substring(0, 1).toUpperCase())
            .append(name.substring(1).toLowerCase())
            .append(space);
        }
        return b.toString();
    }

}
