package wikipedia;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;


import db.Player;
import db.Team;
import db.Timeline;

import info.bliki.api.Connector;
import info.bliki.api.User;
import info.bliki.api.query.OpenSearch;
import info.bliki.api.query.RequestBuilder;

public class OpenSearchWikipedia {

    public static String[] WIKIPEDIA_URL = {"http://it.wikipedia.org/w/api.php","http://en.wikipedia.org/w/api.php"};
    public static String FORMAT_XML = "xml";
    public static int MAX_LENGTH = 5;
    public static String[] DISAMBIGUATORS = {"Dati biografici","Personal information"};
    public static String[] CLASSNAMES = {"sinottico","infobox"};

    public String year = "(19|20)\\d{2}";
    public String teamPattern;

    public String url_result;
    public boolean found = false;
    public Player p;
    public Pattern pattern;
    public Matcher matcher;
    
    public ArrayList<Timeline> playerTimeline;

    public OpenSearchWikipedia() {
        this.found = false;
    }

    public void setPlayer(Player p) {
        this.p = p;
    }

    public void setTeamPattern(String teamPattern) {
        this.teamPattern = teamPattern;
    }

    public void search() throws IOException {

        User user;
        Connector connector;
        RequestBuilder request;
        String playerName = this.p.getName()+" "+this.p.getSurname();

        for (String wiki_url: WIKIPEDIA_URL) {

            user = new User("", "", wiki_url);
            connector = new Connector();
            user = connector.login(user);

            request = OpenSearch.create().search(playerName).format(FORMAT_XML).maxlag(MAX_LENGTH);
            if(!this.found) {
                playerTimeline = new ArrayList<Timeline>();
                loadResultingUrls(connector.sendXML(user, request),wiki_url);                
            }
        }

        if(this.found) {
            //Extract historic soccer teams
            System.out.println(playerName+": "+this.url_result);
            loadHistory(this.url_result);
            this.found = false;
        } else {
            System.out.println(playerName+": NOT FOUND");
        }

    }

    public void loadHistory(String url) throws IOException {
        Elements table;
        Document site = Jsoup.connect(url).get();
        boolean readTeams = false;
        String trString;

        for(String classname : CLASSNAMES) {
            table = site.getElementsByClass(classname);

            //Has elements to iterate
            if(table.size() > 0) {
                Element firstChild = table.get(0).child(0);
                if(firstChild.tagName().compareTo("caption") == 0)
                    firstChild = firstChild.nextElementSibling();

                List<Node> children = firstChild.childNodes();

                for(Node trs : children) {
                    trString = trs.toString();

                    if(readTeams) {
                        processTeam(trs);
                    }
                    if(trString.contains("Years") || trString.contains("Squadre di club")){
                        readTeams = true;
                    }
                }
            }
        }

    }

    private void processTeam(Node node) {
        pattern = Pattern.compile(teamPattern);
        matcher = pattern.matcher(node.toString());
        
        Timeline r_timeline;
        String r_team = "";
        int year_start=0, year_end=0;
        
        

        if(matcher.find()) {

            while(matcher.find())
                r_team = matcher.group();

            pattern = Pattern.compile(year);
            matcher = pattern.matcher(node.toString());

            while (matcher.find()) {
                if(year_start !=0  && Integer.valueOf(matcher.group()) > year_start)
                    year_end = Integer.valueOf(matcher.group());
                if(year_start == 0)
                    year_start = Integer.valueOf(matcher.group());
            }

            if(r_team.length() != 0 && year_start != 0) {
                r_timeline = new Timeline();
                r_timeline.setPlayer(p);
                r_timeline.setTeam(new Team(r_team));
                r_timeline.setYearStart(year_start);
                r_timeline.setYearEnd(year_end);
                
                playerTimeline.add(r_timeline);                
            }
        }

    }

    public void loadResultingUrls(String response,String url) throws IOException {
        Document doc = Jsoup.parse(response, "UTF-8");
        Iterator<Element> items = doc.getElementsByTag("Url").iterator();

        while(items.hasNext()) {
            Element item = items.next();

            //First filter: Check if the resulting web page contains the expect keywords
            //and the url contains the last name of the player
            if(analyzeWebPage(item.text()) && !this.found){
                this.url_result = item.text();
                this.found = true;
            }
        }
    }


    public boolean analyzeWebPage(String url) throws IOException {
        String html = Jsoup.connect(url).get().html();
        for(String disambiguator : DISAMBIGUATORS) {
            if(html.contains(disambiguator)) {
                return true;
            }
        }
        return false;
    }

}
