package basic;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

/**
 * @author Yasser Ganjisaffar <lastname at gmail dot com>
 */
public class BasicCrawlController {

    public static void main(String[] args) throws Exception {
        if (args.length != 2) {
            System.out.println("Needed parameters: ");
            System.out.println("\t rootFolder (it will contain intermediate crawl data)");
            System.out.println("\t numberOfCralwers (number of concurrent threads)");
            return;
        }

        /*
﻿   * crawlStorageFolder is a folder where intermediate crawl data is
﻿   * stored.
﻿   */
        String crawlStorageFolder = args[0];

        /*
﻿   * numberOfCrawlers shows the number of concurrent threads that should
﻿   * be initiated for crawling.
﻿   */
        int numberOfCrawlers = Integer.parseInt(args[1]);

        CrawlConfig config = new CrawlConfig();

        config.setCrawlStorageFolder(crawlStorageFolder);

        /*
﻿   * Be polite: Make sure that we don't send more than 1 request per
﻿   * second (1000 milliseconds between requests).
﻿   */
        config.setPolitenessDelay(1000);

        /*
﻿   * You can set the maximum crawl depth here. The default value is -1 for
﻿   * unlimited depth
﻿   */
        config.setMaxDepthOfCrawling(2);

        /*
﻿   * You can set the maximum number of pages to crawl. The default value
﻿   * is -1 for unlimited number of pages
﻿   */
        config.setMaxPagesToFetch(1000);

        /*
﻿   * Do you need to set a proxy? If so, you can use:
﻿   * config.setProxyHost("proxyserver.example.com");
﻿   * config.setProxyPort(8080);
﻿   * 
﻿   * If your proxy also needs authentication:
﻿   * config.setProxyUsername(username); config.getProxyPassword(password);
﻿   */

        /*
﻿   * This config parameter can be used to set your crawl to be resumable
﻿   * (meaning that you can resume the crawl from a previously
﻿   * interrupted/crashed crawl). Note: if you enable resuming feature and
﻿   * want to start a fresh crawl, you need to delete the contents of
﻿   * rootFolder manually.
﻿   */
        config.setResumableCrawling(false);

        /*
﻿   * Instantiate the controller for this crawl.
﻿   */
        PageFetcher pageFetcher = new PageFetcher(config);
        RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
        RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
        CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);

        /*
﻿   * For each crawl, you need to add some seed urls. These are the first
﻿   * URLs that are fetched and then the crawler starts following links
﻿   * which are found in these pages
﻿   */

        controller.addSeed("http://www.corrieredellosport.it/calcio_mercato/2012/08/31-262520/Fiorentina,+%C3%A8+fatta%3A+preso+Llama+dal+Catania");
        controller.addSeed("http://www.ilmessaggero.it/sport/calcio/calciomercato_roma_goicoechea_borriello_juve_bendtner/notizie/216926.shtml");
        controller.addSeed("http://www.tuttosport.com/calcio/euro_2012/2012/06/19-195026/Juve-Verratti%3A+s%C3%AC.+Al+Pescara+7+milioni+e+due+giovani");
        controller.addSeed("http://www.goal.com/it/news/7/calciomercato/2012/08/24/3327428/la-fiorentina-aspetta-berbatov-c%C3%A8-lofferta-al-manchester");
        
        controller.addSeed("https://it.wikipedia.org/wiki/Lega_Nazionale_Professionisti_Serie_A");
        controller.addSeed("http://fantascudetto.sky.it/index.php?page=calciatori&ruolo=P");
        controller.addSeed("http://fantascudetto.sky.it/index.php?page=calciatori&ruolo=D");
        controller.addSeed("http://fantascudetto.sky.it/index.php?page=calciatori&ruolo=C");
        controller.addSeed("http://fantascudetto.sky.it/index.php?page=calciatori&ruolo=A");
        
        /*
﻿   * Start the crawl. This is a blocking operation, meaning that your code
﻿   * will reach the line after this only when crawling is finished.
﻿   */
        controller.start(BasicCrawler.class, numberOfCrawlers);
    }
}
