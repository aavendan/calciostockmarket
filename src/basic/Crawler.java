package basic;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import wikipedia.CrawlerWikipedia;
import wikipedia.OpenSearchWikipedia;

import db.Player;
import db.PlayerDB;
import db.Timeline;


public class Crawler {

    static ArrayList<Player> list_players;
    static ArrayList<String> teams; 
    static ArrayList<String> names;
    static PlayerDB db = null;
    
    static String DIRECTORY = "/usr/local/calcio";
    static String NAME_DB = "players";

    public static void main(String args[]) {

//        savePlayersDB();
//        showAllPlayers();
//        crawlPlayersOnOpenSearchWikipedia();
        
    }
    
    
    public static void showCurrentTimeline() {
        try{
            db = new PlayerDB(DIRECTORY,NAME_DB);
            
            
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (db != null) {
                try {
                    db.tearDown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static void savePlayersDB() {

        list_players = new ArrayList<Player>();
        teams = new ArrayList<String>();
        names = new ArrayList<String>();

        Crawler.parsePlayers("docs/htmls/fantascudetto.sky.it_index.php?page=calciatori&ruolo=A.txt", 
        "http://fantascudetto.sky.it/index.php?page=calciatori&ruolo=A");
        Crawler.parsePlayers("docs/htmls/fantascudetto.sky.it_index.php?page=calciatori&ruolo=P.txt", 
        "http://fantascudetto.sky.it/index.php?page=calciatori&ruolo=P");
        Crawler.parsePlayers("docs/htmls/fantascudetto.sky.it_index.php?page=calciatori&ruolo=D.txt", 
        "http://fantascudetto.sky.it/index.php?page=calciatori&ruolo=D");
        Crawler.parsePlayers("docs/htmls/fantascudetto.sky.it_index.php?page=calciatori&ruolo=C.txt", 
        "http://fantascudetto.sky.it/index.php?page=calciatori&ruolo=C");
        
        try{
            db = new PlayerDB(DIRECTORY,NAME_DB);
            
            for(Player p : list_players) {
                db.insertPlayer(p);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e1) {
            e1.printStackTrace();
        } finally {
            if (db != null) {
                try {
                    db.tearDown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static void showAllPlayers() {
        ArrayList<Player> players_res;
        try {
            
            db = new PlayerDB(DIRECTORY,NAME_DB);
            
            players_res = db.getAllPlayers();

            for(Player p: players_res) {
                System.out.println(p);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (db != null) {
                try {
                    db.tearDown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static void crawlPlayersOnOpenSearchWikipedia() {
        try {
            
            db = new PlayerDB(DIRECTORY,NAME_DB);
            
            ArrayList<Player> players = db.getAllNotCrawledPlayers();
            ArrayList<String> teams = db.getAllTeams();

            String teamPattern = "(";
            for(String team : teams) {
                teamPattern += team+"|";
            }
            teamPattern = teamPattern.substring(0, teamPattern.length()-1)+")";
            
            OpenSearchWikipedia op = new  OpenSearchWikipedia();
            op.setTeamPattern(teamPattern);
            
            for(Player player : players) {
                op.setPlayer(player);
                op.search();
                for(Timeline tl : op.playerTimeline) {
                    System.out.println(tl);
                    db.insertTimeline(tl);
                }
            }
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (db != null) {
                try {
                    db.tearDown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static void crawlPlayersOnWikipedia() {
        try {
            
            db = new PlayerDB(DIRECTORY,NAME_DB);
            CrawlerWikipedia.CrawlWikipedia(db,db.getAllNotCrawledPlayers());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {
            if (db != null) {
                try {
                    db.tearDown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public static void parsePlayers(String fileLoc, String url) {
        Document doc;
        Player p;
        String team,name;
        try {
            File input = new File(fileLoc);
            doc = Jsoup.parse(input, "UTF-8", url);

            Elements players = doc.getElementsByClass("rosa-giocatore");
            Iterator<Element> playit = players.iterator();
            while(playit.hasNext()) {

                Element player = playit.next();
                if(!player.text().equals("Nome")) {

                    name = player.text();
                    team = player.nextElementSibling().text();
                    p = getPlayer(name,team);

                    list_players.add(p);
                    if(!teams.contains(team))
                        teams.add(team);

                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static Player getPlayer(String name, String team) {
        Player p = new Player();
        int i = 0;

        name = name.replaceAll("'", "");

        for (char c : name.toCharArray()) {
            if (Character.isUpperCase(c) || Character.isSpaceChar(c)) 
                i++;
            else break;
        }

        p.setName(name.substring(i-1, name.length()));
        p.setSurname(name.substring(0, i-1));
        p.setTeam(team);

        return p;
    }

    public static void findNames1(String fileLoc, String url) {
        Document doc;

        try {
            File input = new File(fileLoc);
            doc = Jsoup.parse(input, "UTF-8", url);

            String regex="[A-Z]([a-z]+|\\.)(?:\\s+[A-Z]([a-z]+|\\.))*(?:\\s+[a-z][a-z\\-]+){0,2}\\s+[A-Z]([a-z]+|\\.)";
            Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
            Matcher m = pattern.matcher(doc.body().text());

            System.out.println("======================================================");
            System.out.println(url);
            System.out.println("======================================================");

            String result;
            while (m.find()){
                result = m.group();
                for(Player p : list_players) {
                    if( (result.contains(p.getName()) && result.contains(p.getSurname())) || result.contains(p.getSurname())) {
                        System.out.println(result+" :: "+ p);
                        names.add(result);
                    } 
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void findNames2(String fileLoc, String url) {
        Document doc;

        try {
            File input = new File(fileLoc);
            doc = Jsoup.parse(input, "UTF-8", url);

            String body = doc.body().text();

            System.out.println("======================================================");
            System.out.println(url);
            System.out.println("======================================================");

            int ini,fin;

            for(Player p : list_players) {

                if(body.contains(p.getSurname()) && ( body.contains(p.getName()) || body.contains(p.getTeam()) )){

                    ini = body.indexOf(p.getSurname());
                    fin = body.lastIndexOf(p.getSurname());

                    if(ini > 11) ini -= 10;
                    if(fin < body.length()-p.getSurname().length()-10) fin += p.getSurname().length()+10;

                    System.out.println("("+(ini)+","+(fin)+") :: "+body.substring(ini, fin)+" :: "+p);

                } 
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public static void parseBlogs() {
//      System.out.println("Regex and match with names and surnames");
//      SavePlayers.findNames1("docs/htmls/www.corrieredellosport.it_calcio_mercato_2012_08_31-262520_Fiorentina,+%C3%A8+fatta%3A+preso+Llama+dal+Catania.txt", 
//      "http://www.corrieredellosport.it/calcio_mercato/2012/08/31-262520/Fiorentina,+%C3%A8+fatta%3A+preso+Llama+dal+Catania");
//      SavePlayers.findNames1("docs/htmls/www.ilmessaggero.it_sport_calcio_calciomercato_roma_goicoechea_borriello_juve_bendtner_notizie_216926.shtml.txt", 
//      "http://www.ilmessaggero.it/sport/calcio/calciomercato_roma_goicoechea_borriello_juve_bendtner/notizie/216926.shtml");
//      SavePlayers.findNames1("docs/htmls/www.goal.com_it_news_7_calciomercato_2012_08_24_3327428_la-fiorentina-aspetta-berbatov-c%C3%A8-lofferta-al-manchester.txt", 
//      "http://www.goal.com/it/news/7/calciomercato/2012/08/24/3327428/la-fiorentina-aspetta-berbatov-c%C3%A8-lofferta-al-manchester");
//      SavePlayers.findNames1("docs/htmls/www.tuttosport.com_calcio_euro_2012_2012_06_19-195026_Juve-Verratti%3A+s%C3%AC.+Al+Pescara+7+milioni+e+due+giovani.txt", 
//      "http://www.tuttosport.com/calcio/euro_2012/2012/06/19-195026/Juve-Verratti%3A+s%C3%AC.+Al+Pescara+7+milioni+e+due+giovani");
//
//      
//      
//      System.out.println("Looking for names in the raw");
//      SavePlayers.findNames2("docs/htmls/www.corrieredellosport.it_calcio_mercato_2012_08_31-262520_Fiorentina,+%C3%A8+fatta%3A+preso+Llama+dal+Catania.txt", 
//      "http://www.corrieredellosport.it/calcio_mercato/2012/08/31-262520/Fiorentina,+%C3%A8+fatta%3A+preso+Llama+dal+Catania");
//      SavePlayers.findNames2("docs/htmls/www.ilmessaggero.it_sport_calcio_calciomercato_roma_goicoechea_borriello_juve_bendtner_notizie_216926.shtml.txt", 
//      "http://www.ilmessaggero.it/sport/calcio/calciomercato_roma_goicoechea_borriello_juve_bendtner/notizie/216926.shtml");
//      SavePlayers.findNames2("docs/htmls/www.goal.com_it_news_7_calciomercato_2012_08_24_3327428_la-fiorentina-aspetta-berbatov-c%C3%A8-lofferta-al-manchester.txt", 
//      "http://www.goal.com/it/news/7/calciomercato/2012/08/24/3327428/la-fiorentina-aspetta-berbatov-c%C3%A8-lofferta-al-manchester");
//      SavePlayers.findNames2("docs/htmls/www.tuttosport.com_calcio_euro_2012_2012_06_19-195026_Juve-Verratti%3A+s%C3%AC.+Al+Pescara+7+milioni+e+due+giovani.txt", 
//      "http://www.tuttosport.com/calcio/euro_2012/2012/06/19-195026/Juve-Verratti%3A+s%C3%AC.+Al+Pescara+7+milioni+e+due+giovani");

    }
    
}
