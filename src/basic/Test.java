package basic;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import db.PlayerDB;

public class Test {

    static String DIRECTORY = "/usr/local/calcio";
    static String NAME_DB = "players";

    public static void main(String args[]) {
        try {
            PlayerDB db = new PlayerDB(DIRECTORY,NAME_DB);

            String listTeams = "(";
            String year = "(19|20)\\d{2}";
            
            ArrayList<String> teams = db.getAllTeams();
            for(String team : teams) {
                listTeams += team+"|";
            }
            
            listTeams = listTeams.substring(0, listTeams.length()-1)+")";
            
            Pattern pattern = Pattern.compile(listTeams);

            String text = "La Roma contra Lazioakldfjaslkfjas ajfdljafdljasf aljfdlasfj Roma fjalkfdjaslfjd Juventus";
            Matcher matcher = pattern.matcher(text);

            while (matcher.find()) {
                System.out.print("Start index: " + matcher.start());
                System.out.print(" End index: " + matcher.end() + " ");
                System.out.println(matcher.group());
            }
            
            text = "1998-2001 jaslfkjlsadjf ajflsajfdlkasjfd ajldfjaslfdj 34 342 4334";
            
            pattern = Pattern.compile(year);
            matcher = pattern.matcher(text);
            
            while (matcher.find()) {
                System.out.print("Start index: " + matcher.start());
                System.out.print(" End index: " + matcher.end() + " ");
                System.out.println(matcher.group());
            }
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
