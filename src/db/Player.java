package db;

public class Player {
    private int id;
    private String name;
    private String surname;
    private String team;
    private boolean crawled;
    
    public String getTeam() {
        return team;
    }
    public void setTeam(String team) {
        this.team = team;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        
        this.surname = titleCase(surname);
    }
    
    public static String titleCase(String realName) {
        String space = " ";
        String[] names = realName.split(space);
        StringBuilder b = new StringBuilder();
        for (String name : names) {
            if (name == null || name.isEmpty()) {
                b.append(space);
                continue;
            }
            b.append(name.substring(0, 1).toUpperCase())
                    .append(name.substring(1).toLowerCase())
                    .append(space);
        }
        return b.toString();
    }
    
    public void setCrawled(boolean crawled) {
        this.crawled = crawled;
    }
    public boolean isCrawled() {
        return crawled;
    }
    
    public String toString() {
        return this.id+"::"+this.name+" "+this.surname+"("+this.team+")"+"-"+this.crawled;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }
}
