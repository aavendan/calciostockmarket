package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;


public class PlayerDB {

    private Connection fConnection;
    private PreparedStatement fInsertPlayer;
    private PreparedStatement fSelectAllPlayer;
    private PreparedStatement fInsertTeam;
    private PreparedStatement fGetTeamId;
    private PreparedStatement fGetTeamName;
    private PreparedStatement fGetPlayer;
    private PreparedStatement fUpdatePlayer;
    private PreparedStatement fSelectAllNotCrawledPlayer;
    private PreparedStatement fGetTeams;
    private PreparedStatement fInsertTimeline;
    private PreparedStatement fSelectTimeline;
    private PreparedStatement fSelectTimelineByPlayer;
    private PreparedStatement fUpdateTimeline;

    public PlayerDB(String directory, String databaseSubdirectoryName) throws Exception{

        Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        Properties properties = new Properties();
        properties.put("user", "user1");
        properties.put("password", "user1");
        String jdbcUrl;

        if (directory.charAt(directory.length() - 1) == '/') {
            jdbcUrl = "jdbc:derby:" + directory + databaseSubdirectoryName + ";create=true;characterEncoding=utf-8";
        } else {
            jdbcUrl = "jdbc:derby:" + directory + "/" + databaseSubdirectoryName + ";create=true;characterEncoding=utf-8";
        }

        fConnection = DriverManager.getConnection(jdbcUrl, properties);
        createTableIfItDoesntExist();
        
        fInsertPlayer = fConnection.prepareStatement("INSERT INTO player (player_name, player_surname, team_id) VALUES (?,?,?)");
        fSelectAllPlayer = fConnection.prepareStatement("SELECT * FROM player");
        fSelectAllNotCrawledPlayer = fConnection.prepareStatement("SELECT * FROM player where player_crawled = 'false'");
        fGetPlayer = fConnection.prepareStatement("SELECT * FROM player  WHERE player_name = ? AND player_surname = ?");
        fUpdatePlayer = fConnection.prepareStatement("UPDATE player SET player_crawled = ?  WHERE player_name = ? AND player_surname = ?");
        
        fInsertTeam = fConnection.prepareStatement("INSERT INTO team (team_name) VALUES (?)");
        fGetTeamId = fConnection.prepareStatement("SELECT team_id FROM team WHERE team_name = ?");
        fGetTeamName = fConnection.prepareStatement("SELECT team_name FROM team WHERE team_id = ?");
        fGetTeams = fConnection.prepareStatement("SELECT team_name FROM team");
        
        fInsertTimeline = fConnection.prepareStatement("INSERT INTO timeline(player_id,team_id,year_ini,year_fin) VALUES (?,?,?,?)");
        fUpdateTimeline = fConnection.prepareStatement("UPDATE timeline SET year_fin = ?  WHERE player_id = ? AND team_id = ? AND year_ini = ?");
        fSelectTimeline = fConnection.prepareStatement("SELECT * FROM timeline WHERE player_id = ? AND team_id = ? AND year_ini = ?");
        fSelectTimelineByPlayer = fConnection.prepareStatement("SELECT * FROM timeline WHERE player_id = ?");
    }

    private void createTableIfItDoesntExist() throws Exception {

        ResultSet resultSet = fConnection.getMetaData().getTables("%", "%", "%", new String[] { "TABLE" });
        boolean shouldCreateTablePlayer = true;
        boolean shouldCreateTableTeam = true;
        boolean shouldCreateTableTimeline = true;

        String tableName = null;
        while (resultSet.next()) {
            tableName = resultSet.getString("TABLE_NAME");
            if (tableName.equalsIgnoreCase("PLAYER")) {
                shouldCreateTablePlayer = false;
            } else if (tableName.equalsIgnoreCase("TEAM")) {
                shouldCreateTableTeam = false;
            } else if (tableName.equalsIgnoreCase("TIMELINE")) {
                shouldCreateTableTimeline = false;
            }
        }
        resultSet.close();
        if (shouldCreateTableTeam) {
            System.out.println("Creating Table team...");
            Statement statement = fConnection.createStatement();
            statement.execute("CREATE TABLE team "
                    + "(team_id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),"
                    + " team_name VARCHAR(256) NOT NULL "
                    + " ) ");
            statement.execute("CREATE INDEX indx_team ON team(team_name)");
            statement.execute("ALTER TABLE team ADD CONSTRAINT fk_team PRIMARY KEY (team_id)");
            statement.close();
        }
        if (shouldCreateTablePlayer) {
            System.out.println("Creating Table player...");
            Statement statement = fConnection.createStatement();
            statement.execute("CREATE TABLE player "
                    + "(player_id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),"
                    + " player_name VARCHAR(256) NOT NULL, "
                    + " player_surname VARCHAR(256) NOT NULL, "
                    + " team_id INTEGER,"
                    + " player_crawled BOOLEAN DEFAULT FALSE NOT NULL "
                    + " ) ");
            statement.execute("CREATE INDEX indx_player ON player(player_name,player_surname)");
            statement.execute("ALTER TABLE player ADD CONSTRAINT fk_player PRIMARY KEY (player_id)");
            statement.execute("ALTER TABLE player ADD CONSTRAINT fk_player_team FOREIGN KEY (team_id) REFERENCES team(team_id)");
            statement.close();
        }
        if(shouldCreateTableTimeline) {
            System.out.println("Creating Table timeline...");
            Statement statement = fConnection.createStatement();
            statement.execute("CREATE TABLE timeline "
                    + "(timeline_id INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),"
                    + " player_id INTEGER NOT NULL, "
                    + " team_id INTEGER NOT NULL, "
                    + " year_ini INTEGER NOT NULL,"
                    + " year_fin INTEGER"
                    + " ) ");
            statement.execute("CREATE INDEX indx_timeline ON timeline(player_id,team_id,year_ini)");
            statement.execute("ALTER TABLE timeline ADD CONSTRAINT fk_timeline PRIMARY KEY (timeline_id)");
            statement.execute("ALTER TABLE timeline ADD CONSTRAINT fk_timeline_team   FOREIGN KEY (team_id) REFERENCES team(team_id)");
            statement.execute("ALTER TABLE timeline ADD CONSTRAINT fk_timeline_player FOREIGN KEY (player_id) REFERENCES player(player_id)");
            statement.close();
        }
    }
    
    public void tearDown() throws Exception {
       fConnection.close();
    }
    
    public void insertPlayer(Player player) throws Exception {
        if(!existsPlayer(player)) {
            int team = insertTeam(player.getTeam());
//            System.out.println("Inserting: "+player.getName()+" "+player.getSurname()+" team "+player.getTeam()+" "+team);
            fInsertPlayer.setString(1, player.getName());
            fInsertPlayer.setString(2, player.getSurname());
            fInsertPlayer.setInt(3, team);
            fInsertPlayer.execute();
        }
    }
    
    public boolean existsPlayer(Player player) throws Exception {
        fGetPlayer.setString(1, player.getName());
        fGetPlayer.setString(2, player.getSurname());
        ResultSet resultSet = fGetPlayer.executeQuery();
        try {
            if (resultSet.next()) {
                return true;
            }
        } finally {
            resultSet.close();
        }
        return false;
    }
    
    public int insertTeam(String team) throws Exception {
        int code = getTeamId(team);
        if( code == -1) {
            fInsertTeam.setString(1, team);
            fInsertTeam.execute();
            code = getTeamId(team);
        }
        return code;
    }
    
    public int getTeamId(String name) throws Exception {
        fGetTeamId.setString(1, name);
        ResultSet resultSet = fGetTeamId.executeQuery();
        try {
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } finally {
            resultSet.close();
        }
        return -1;
    }
    
    public ArrayList<String> getAllTeams() throws Exception {
        ArrayList<String> teams = new ArrayList<String>();
        ResultSet resultSet = fGetTeams.executeQuery();
        
        try {
            while (resultSet.next()) {
                teams.add(resultSet.getString(1));
            }
        } finally {
            resultSet.close();
        }
        return teams;
    }
    
    public String getTeamName(int id) throws Exception {
        fGetTeamName.setInt(1, id);
        ResultSet resultSet = fGetTeamName.executeQuery();
        try {
            if (resultSet.next()) {
                return resultSet.getString(1);
            }
        } finally {
            resultSet.close();
        }
        return null;
    }

    public ArrayList<Player> getAllPlayers() throws Exception {
        ResultSet resultSet = fSelectAllPlayer.executeQuery();
        
        if(resultSet != null) {
            ArrayList<Player> players = new ArrayList<Player>();
            Player p;
            while(resultSet.next()) {
                p = new Player();
                p.setId(resultSet.getInt(1));
                p.setName(resultSet.getString(2));
                p.setSurname(resultSet.getString(3));
                p.setTeam(getTeamName(resultSet.getInt(4)));
                p.setCrawled(resultSet.getBoolean(5));
                players.add(p);
            }
            
            resultSet.close();
            
            return players;
        }
        return null;
    }
    
    public ArrayList<Player> getAllNotCrawledPlayers() throws Exception {
        ResultSet resultSet = fSelectAllNotCrawledPlayer.executeQuery();
        
        if(resultSet != null) {
            ArrayList<Player> players = new ArrayList<Player>();
            Player p;
            while(resultSet.next()) {
                p = new Player();
                p.setId(resultSet.getInt(1));
                p.setName(resultSet.getString(2));
                p.setSurname(resultSet.getString(3));
                p.setTeam(getTeamName(resultSet.getInt(4)));
                p.setCrawled(resultSet.getBoolean(5));
                players.add(p);
            }
            
            resultSet.close();
            
            return players;
        }
        return null;
    }
    
    public void updatePlayer(Player player) throws Exception {
        fUpdatePlayer.setBoolean(1, true);
        fUpdatePlayer.setString(2, player.getName());
        fUpdatePlayer.setString(3, player.getSurname());
        fUpdatePlayer.execute();
    }
    
    public void insertTimeline(Timeline tl) throws Exception {
        Player p; 
        Team t; 
        int yearStart, yearEnd;
        
        p = tl.getPlayer();
        t = tl.getTeam();
        yearStart = tl.getYearStart();
        yearEnd = tl.getYearEnd();
        
        if(!existTimeline(p,t,yearStart)) {
            fInsertTimeline.setInt(1, p.getId());
            fInsertTimeline.setInt(2, getTeamId(t.getName()));
            fInsertTimeline.setInt(3, yearStart);
            fInsertTimeline.setInt(4, yearEnd);
            fInsertTimeline.execute();
            updatePlayer(tl.getPlayer());
        } else {
            updateTimeline(tl);
        }
    }
    
    public boolean existTimeline(Player p, Team t, int year_ini) throws Exception {
        fSelectTimeline.setInt(1, p.getId());
        fSelectTimeline.setInt(2, getTeamId(t.getName()));
        fSelectTimeline.setInt(3, year_ini);
        
        ResultSet resultSet =  fSelectTimeline.executeQuery();
        try {
            if (resultSet.next()) {
                return true;
            }
        } finally {
            resultSet.close();
        }
        return false;        
    }
    
    public void updateTimeline(Timeline timeline) throws Exception {
        fUpdateTimeline.setInt(1, timeline.getYearEnd());
        fUpdateTimeline.setInt(2, timeline.getPlayer().getId());
        fUpdateTimeline.setInt(3, getTeamId(timeline.getTeam().getName()));
        fUpdateTimeline.setInt(4, timeline.getYearStart());
        fUpdateTimeline.execute();
    }
    
    
    public ArrayList<Timeline> getAllTimeline() throws Exception {
        ArrayList<Timeline> timeline = new ArrayList<Timeline>();
        ResultSet resultSet = fGetTeams.executeQuery();
        Timeline tl = new Timeline();
        
        try {
            while (resultSet.next()) {
                timeline.add(tl);
            }
        } finally {
            resultSet.close();
        }
        return timeline;
    }
}
