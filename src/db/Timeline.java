package db;

public class Timeline {

    private Player player;
    private Team team;
    private int year_start;
    private int year_end;

    public Player getPlayer() {
        return this.player;
    }
    
    public Team getTeam() {
        return this.team;
    }
    
    public int getYearStart() {
        return year_start;
    }

    public void setYearStart(int year_start) {
        this.year_start = year_start;
    }

    public int getYearEnd() {
        return year_end;
    }

    public void setYearEnd(int year_end) {
        this.year_end = year_end;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public String toString() {
        return "("+this.getPlayer().getName()+","+this.getTeam().getName()+","+this.getYearStart()+","+this.getYearEnd()+")";
    }
}
